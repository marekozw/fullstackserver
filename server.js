(function () {
  'use strict';

  var express      = require('express'),
      bodyParser   = require('body-parser'),
      cors         = require('cors'),
      
      mongoose     = require('mongoose'),
      passport     = require('passport'),

      app    = express(),
      router = express.Router(),

      database = require('./config/database'),

      PORT = 5000;

  // Configuration
  mongoose.connect(database.url);
  require('./config/passport')(passport);

  // App configuration
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());
  app.use(cors({ origin: 'http://localhost:4000' }));
  app.use(passport.initialize());
  
  // Routes
  require('./app/routes/routes')(app, router, passport);

  // Starting server
  app.listen(PORT, function () {
    console.log('Server is ready on localhost:' + PORT);
  });

})();
