(function () {
  'use strict';

  var isAuthorizedMiddleware = function (passport) {
    
    return function (req, res, next) {
      var credentials = {};

      if (req.headers && req.headers['x-credentials']) {
        credentials = JSON.parse(req.headers['x-credentials']);
      }

      req.body.email = req.body.email || credentials.email;
      req.body.password = req.body.password || credentials.password;

      passport.authenticate('local-login', function (error, user, info) {
        if (error) {
          return next(error);
        }

        if (!user) {
          return res.status(401).json(info);
        }

        req.body.user = user;
        next();
      })(req, res, next);
    };
    
  };

  module.exports = isAuthorizedMiddleware;
})();
