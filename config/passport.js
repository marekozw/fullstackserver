(function () {
  'use strict';

  var LocalStrategy = require('passport-local').Strategy,
      User = require('./../app/models/user');

  var passportConfig = function (passport) {

    // Singin Strategy
    passport.use('local-signin', new LocalStrategy({

      usernameField: 'email',
      passwordField: 'password'

    }, function (email, password, done) {
      
      User.findOne({ email: email }, function (err, user) {
        if (err) {
          return done(err);
        }

        if (user) { // email is already taken
          return done(null, false, { flash: 'That email is already taken' });
        }

        var newUser = new User();
        newUser.email = email;
        newUser.password = password;

        newUser.save(function (saveErr) {
          if (saveErr) {
            throw saveErr;
          }

          return done(null, newUser);
        });

      });

    }));
    
    // Login Strategy
    passport.use('local-login', new LocalStrategy({

      usernameField: 'email',
      passwordField: 'password'

    }, function (email, password, done) {

      User.findOne({ email: email }, function (err, user) {

        if (err) {
          return done(err);
        }

        if (!user) {
          return done(null, false, { flash: 'No user found' });
        }

        if (!user.comparePassword(password)) {
          return done(null, false, { flash: 'Incorect password' });
        }

        return done(null, user);
      });

    }));
  };


  module.exports = passportConfig;
})();
