(function () {
  'use strict';

  var host = 'localhost',
      dbName = 'fullstack-db';

  var databaseConfig = {
    url: 'mongodb://' + host + '/' + dbName
  };

  module.exports = databaseConfig;
  
})();
