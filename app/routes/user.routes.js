(function () {
  'use strict';

  var userCtrl = require('./../controllers/user.controller');
  var isAuthorizedMiddleware = require('./../../config/auth');

  var userRoutesConfig = function (expressRouter, passport) {

    var isAuthorized = isAuthorizedMiddleware(passport);

    expressRouter.get('/user', isAuthorized, function (req, res) {
      setTimeout(function () {
        res.json(userCtrl.getAllUsers());
      }, 3000);
    });

  };

  module.exports = userRoutesConfig;

})();
