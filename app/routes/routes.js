(function () {
  'use strict';

  var API_URL = '/api';
  var routesConfig = function (expressApp, expressRouter, passport) {
    
    // All routes congituration
    require('./user.routes')(expressRouter, passport);
    require('./product.routes')(expressRouter, passport);
    require('./auth.routes')(expressRouter, passport);

    expressApp.use(API_URL, expressRouter);
  };

  module.exports = routesConfig;

})();
