(function () {
  'use strict';

  var isAuthorizedMiddleware = require('./../../config/auth');

  var authRoutesConfig = function (expressRouter, passport) {
    
    var authCtrl = require('./../controllers/auth.controller')(passport);
    var isAuthorized = isAuthorizedMiddleware(passport);

    expressRouter.post('/auth/signin', authCtrl.signin);
    expressRouter.post('/auth/login', authCtrl.login);      
    expressRouter.get('/auth/logout', isAuthorized, authCtrl.logout);

  };

  module.exports = authRoutesConfig;

})();
