(function () {
  'use strict';

  var productCtrl = require('./../controllers/product.controller');
  var isAuthorizedMiddleware = require('./../../config/auth');

  var productRoutesConfig = function (expressRouter, passport) {

    var isAuthorized = isAuthorizedMiddleware(passport);

    expressRouter.get('/products', isAuthorized, function (req, res) {
      res.json(productCtrl.getAllProducts());
    });

  };

  module.exports = productRoutesConfig;
  
})();
