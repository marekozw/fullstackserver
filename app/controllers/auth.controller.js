(function () {
  'use strict';

  var authFactoryMethod = function (passport) {
    
    var authCtrl = {

      signin: function (req, res, next) {
        passport.authenticate('local-signin', function (error, user, info) {
          if (error) {
            return next(error);
          }

          if (!user) {
            return res.status(422).json(info);
          }

          req.logIn(user, { session: false }, function (err) {
            if (err) {
              return next(err);
            }

            return res.send(user.toJSON());
          });

        })(req, res, next);
      },

      login: function (req, res, next) {
        passport.authenticate('local-login', function (error, user, info) {
          if (error) {
            return next(error);
          }

          if (!user) {
            return res.status(401).json(info);
          }

          req.logIn(user, { session: false }, function (err) {
            if (err) {
              return next(err);
            }

            return res.send(user.toJSON());
          });

        })(req, res, next);
      },

      logout: function (req, res) {
        req.logout();
        res.json({ flash: 'Logged out' });
      }

    };

    return authCtrl;
  };

  module.exports = authFactoryMethod;

})();
