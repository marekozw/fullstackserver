(function () {
  'use strict';
  
  var productController = {
    
    getAllProducts: function () {
      return [
        { id: 0, name: 'Product 0'},
        { id: 1, name: 'Product 1'},
        { id: 2, name: 'Product 2'}
      ];
    }

  };

  module.exports = productController;

})();
